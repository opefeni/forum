@extends('layout.master')

@section('head')
	@parent
	<title>Login</title>
@stop

@section('content')
	<div class="container">
		<h1>Login</h1>
		@if(Session::has('success'))
		<div class="alert alert-success"> {{ Session::get('success')}}</div>
	@elseif(Session::has('fail'))
		<div class="alert alert-danger"> {{ Session::get('fail')}} </div>
	@endif
			<form role="form" method="post" action="{{ URL::route('postLogin')}}">
				<div class="form-group {{ ($errors->has('username')) ? 'has_error' : ''}}">
						<label for="username">Username</label>
						<input type="text" id="username" name="username" class="form-control" >
						@if($errors->has('username'))
							{{ $errors->first('username')}}
						@endif
				</div>
				<div class="form-group {{ ($errors->has('pass1')) ? 'has_error' : ''}}" >
						<label for="pass1">Password</label>
						<input type="password" id="pass1" name="pass1" class="form-control" >
						@if($errors->has('pass1'))
							{{ $errors->first('pass1')}}
						@endif
				</div>
				<div class="form-group">
						<input type="checkbox" name="remember" id="remember"> Remember me
				</div>
				{{Form::token()}}
				<div class="form-group">
						<input type="submit" value="Login" class="btn btn-default" >
				</div>

			</form>
	</div>
@stop