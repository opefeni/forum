@extends('layout.master')

@section('head')
	@parent
	<title>Register</title>
@stop

@section('content')
	<div class="container">
		<h1>Register</h1>
			<form role="form" method="post" action="{{ URL::route('postCreate')}}">
				<div class="form-group {{ ($errors->has('fullname')) ? 'has_error' : ''}}">
						<label for="fullname">Full Name</label>
						<input type="text" id="fullname" name="fullname" class="form-control" >
						@if($errors->has('fullname'))
							{{ $errors->first('fullname')}}
						@endif
				</div>
				<div class="form-group {{ ($errors->has('username')) ? 'has_error' : ''}}">
						<label for="username">Username</label>
						<input type="text" id="username" name="username" class="form-control" >
						@if($errors->has('username'))
							{{ $errors->first('username')}}
						@endif
				</div>
				<div class="form-group {{ ($errors->has('pass1')) ? 'has_error' : ''}}" >
						<label for="pass1">Password</label>
						<input type="password" id="pass1" name="pass1" class="form-control" >
						@if($errors->has('pass1'))
							{{ $errors->first('pass1')}}
						@endif
				</div>
				<div class="form-group {{ ($errors->has('pass2')) ? 'has_error' : ''}}">
						<label for="pass2">Confirm Password</label>
						<input type="password" id="pass2" name="pass2" class="form-control" >
						@if($errors->has('pass2'))
							{{ $errors->first('pass2')}}
						@endif
				</div>
				{{Form::token()}}
				<div class="form-group">
						<input type="submit" value="Register" class="btn btn-default" >
				</div>

			</form>
	</div>
@stop