<!doctype html>
<html lang="en">
<head>
	@section('head')
	<meta charset="UTF-8">
	<link rel="stylesheet" href="{{asset('bootstrap.min.css')}}">
    @show	
</head>
<body>
	
	<div class="navbar">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href='{{ URL::route('getHome')}}' class="navbar-brand"><hstrong>VASCON Forum</strong></a>
				</div>
				<div class="navbar-collapse collapse navbar-responsive-collapse">
						<ul class="nav navbar-nav">
								<li><a href='{{ URL::route('getHome')}}' >Home</a></li>
								<li><a href='{{ URL::route('forum-home')}}' >Forum</a></li>

						</ul>
						<ul class="nav navbar-nav navbar-right">
							@if(!Auth::check())
								<li><a href='{{ URL::route('getCreate')}}' >Register</a></li>
								<li><a href='{{ URL::route('getLogin')}}' >login</a></li>
							@else
								<li><a href='{{ URL::route('getCreate')}}' >My Profile({{ Auth::user()->fullname}}) </a></li>
								<li><a href='{{ URL::route('getLogout')}}' >logout</a></li>
							@endif
						</ul>
				</div>

			</div>
	</div>
	<div class="container">
		@if(Session::has('success'))
			<div class="alert alert-success"> {{ Session::get('success')}}</div>
		@elseif(Session::has('fail'))
			<div class="alert alert-danger"> {{ Session::get('fail')}} </div>
		@endif

		@yield('content')
	</div>
	@section('javascript')
	<script src="{{asset('jquery.min.js')}}" ></script>
	<script src="{{asset('bootstrap.min.js')}}" /></script>
	@show
</body>
</html>
