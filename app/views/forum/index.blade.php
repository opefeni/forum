@extends('layout.master');

@section('head')
	@parent
	<title>Discussion Forums</title>
@stop

@section('content')
	@if(Auth::check() && Auth::user()->isAdmin)
	<div>
		<a href="#" class="btn btn-default" data-toggle="modal" data-target="#group_form">Add Group</a>
	</div>
	@endif

	@foreach($groups as $group)
		<div class="panel panel-default">
		  <div class="panel-heading">
		  	<div class="clearfix">
		    	<h3 class="panel-title pull-left">{{$group->title}}</h3>
		    	@if(Auth::check() && Auth::user()->isAdmin)
		    	<a id="{{ $group->id }}" data-toggle="modal" data-target="#group_delete" class="btn btn-danger pull-right btn-xs delete_group">Delete </a>
		    	<a id="{{ $group->id }}" data-toggle="modal" data-target="#categoryForm" class="btn btn-primary pull-right btn-xs new_category">New Category </a>  

				@endif
			</div>
		  </div>
		  <div class="panel-body">
		  	<div class="list-group">
		    	@foreach($category as $cat)
		    		@if($cat->group_id == $group->id)
					  <a href="{{ URL::route('forum-category',$cat->id)}}" class="list-group-item">{{ $cat->title }}</a>
		    		@endif
		    	@endforeach
		    </div>
		  </div>
		</div>
	@endforeach

	@if(Auth::check() && Auth::user()->isAdmin)

	<!-- beginning of modal window for adding group -->
	<div class="modal fade" role="dialog" aria-hidden="true" tabindex="-1" id="group_form">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span>
							<span class="sr-only"> Close </span>
						</button>
						<h4 class="modal-title"> New Group</h4>

				</div>
				<div class="modal-body">
						<form id="target_form" method="post" action=" {{ URL::route('addGroup')}}">
								<div class="form-group{{ ($errors->has('group_name')) ? 'has_error' : ''}}">
										<label for="group_name"> Group Name: </label>
										<input type="text" id="group_name" name="group_name" class="form-control">
										@if($errors->has('group_name'))
											<p> {{ $errors->first('group_name')}}</p>
										@endif
								</div>
							{{Form::token()}}
						</form>
				</div>
				<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal" id="form_submit">Save</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!--end of modal window for adding group

	<!-- beginning of modal window for adding category -->
	<div class="modal fade" role="dialog" aria-hidden="true" tabindex="-1" id="categoryForm">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span>
							<span class="sr-only"> Close </span>
						</button>
						<h4 class="modal-title"> New Group</h4>

				</div>
				<div class="modal-body">
						<form id="category_form" method="post" action="{{ URL::route('addCategory')}}">
								<div class="form-group {{ ($errors->has('category_name')) ? 'has_error' : ''}}">
										<label for="category_name"> Category Name: </label>
										<input type="text" id="category_name" name="category_name" class="form-control">
										@if($errors->has('category_name'))
											<p> {{ $errors->first('category_name')}}</p>
										@endif
										<input type="hidden" id="group_id" name="group_id"  >
								</div>
							{{Form::token()}}
						</form>
				</div>
				<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal" id="category_submit">Save</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!--end of modal window for adding group

	beginning of modal window for group deletion -->
	<div class="modal fade" role="dialog" aria-hidden="true" tabindex="-1" id="group_delete">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span>
							<span class="sr-only"> Close </span>
						</button>
						<h4 class="modal-title"> Delete Group</h4>

				</div>
				<div class="modal-body">
						<h4>Are you sure you want to delete this group</h4>
				</div>
				<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<a href="#" class="btn btn-primary" id="btnDeleteGroup"  >Delete</a>
				</div>
			</div>
		</div>
	</div>
	@endif

@stop
@section('javascript')
	@parent
	<script type="text/javascript" src="/js/app.js"></script>
	
	@if(Session::has('modal'))
		<script type="text/javascript">

		$('{{ Session::get('modal')}}').modal('show');
		</script>
	@endif
	
@stop