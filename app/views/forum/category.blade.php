@extends('layout.master')

@section('head')
	@parent
	<title>Forum | {{$category->title}}</title>
@stop

@section('content')
	@if(Auth::check())
	<div>
		<a href="{{ URL::route('addThread',$category->id)}}" class="btn btn-default">Add Thread</a>
	</div>
	@endif
		<div class="panel panel-default">
		  <div class="panel-heading">
		  	<div class="clearfix">
		    	<h3 class="panel-title pull-left">{{$category->title}}</h3>
		    	@if(Auth::check() && Auth::user()->isAdmin)
		    	<a id="{{ $category->id }}" data-toggle="modal" data-target="#category_delete" class="btn btn-danger pull-right btn-xs delete_category">Delete </a>
				<a href="{{ URL::route('forum-thread',$category->id);}}" class="btn btn-success btn-xs pull-right">New Thread</a>
			
				@else
				<a href="{{ URL::route('forum-thread',$category->id);}}" class="btn btn-success btn-xs pull-right">New Thread</a>
				@endif
			</div>
		  </div>
		  <div class="panel-body">
		  	<div class="list-group">
		    	@foreach($threads as $thread)
					  <a href="{{ URL::route('forum-thread-view',$thread->id)}}" class="list-group-item">{{ $thread->title }}</a>
		    	@endforeach
		    </div>
		  </div>
		</div>
	@if(Auth::check() && Auth::user()->isAdmin)

	<!--beginning of modal window for category deletion -->
	<div class="modal fade" role="dialog" aria-hidden="true" tabindex="-1" id="category_delete">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span>
							<span class="sr-only"> Close </span>
						</button>
						<h4 class="modal-title"> Delete Category</h4>

				</div>
				<div class="modal-body">
						<h4>Are you sure you want to delete this category</h4>
				</div>
				<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<a  class="btn btn-primary" id="btnDeleteCategory">Delete</a>
				</div>
			</div>
		</div>
	</div>
	<!--end of modal window for deleting category -->
	@endif
	
@stop

@section('javascript')
	@parent
	<script type="text/javascript" src="/js/app.js"></script>

@stop
