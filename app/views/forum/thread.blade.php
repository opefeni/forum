@extends('layout.master')
@section('head')
	@parent
	<title> Forum | {{ $thread->title }}</title>
@stop

@section('content')
	
	<ol class="breadcrumb">
	  <li><a href="{{ URL::route('forum-home')}}">Forum</a></li>
	  <li><a href="{{ URL::route('forum-category', $thread->category_id)}}">{{ $thread->category->title}}</a></li>
	  <li class="active">{{$thread->title}}</li>
	</ol>
	<div class="well">
		<h1> {{ $thread->title}}</h1>
		<h4> By {{ $author }} on {{ $thread->created_at}}</h4>

		<hr>
		<p> {{ nl2br($thread->body) }}</p>

	</div>
	<h2>Comments:</h2>
	@foreach($thread->comment()->get() as $comments)
		<div class="well">
			<h4> By {{$comments->author->username}} on  {{ $comments->created_at}}</h4>
			<hr>
			<p> {{ nl2br($comments->body) }}</p>
		</div>
	@endforeach

	@if(Auth::check())
		<form action="{{ URL::route('addComment',$thread->id)}}" method="post">
			<div class="form-group">
				<label for="body"> Body </label>
				<textarea class="form-control" name="body"  id="body"></textarea>
			</div>
			{{Form::token()}}
			<div class="form-group">
				<input type="submit" value="Save Thread" class="btn btn-primary">
			</div>
	</form>
	@endif

@stop