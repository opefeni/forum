<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('uses' => 'HomeController@hello','as' => 'getHome'));

Route::group(array('prefix'=>'/forum'), function(){

	Route::get('/', array('uses'=>'ForumController@index', 'as' => 'forum-home'));
	Route::get('/category/{id}', array('uses'=>'ForumController@category', 'as'=>'forum-category'));
	Route::get('/thread/{id}', array('uses' =>'ForumController@thread', 'as' => 'forum-thread' ));
	Route::get('/thread/view/{id}', array('uses' =>'ForumController@threadView', 'as' => 'forum-thread-view' ));

	Route::group(array('before'=>'admin'), function()
	{
		Route::get('/group/delete/{id}', array('uses'=>'ForumController@deleteGroup', 'as' => 'deleteGroup'));
		Route::get('/category/delete/{id}', array('uses'=>'ForumController@deletecategory', 'as' => 'deleteCategory'));

		Route::group(array('before'=>'csrf'), function()
		{
			Route::post('/group', array('uses'=>'ForumController@addGroup', 'as' => 'addGroup'));
			Route::post('/category',array('uses' => 'ForumController@addCategory', 'as' => 'addCategory'));
		});
	});

	Route::group(array('before'=>'csrf'), function(){

		//Thread section
		Route::get('/forum/thread/delete/{id}', array('uses'=>'ForumController@deleteThread', 'as' => 'deleteThread'));

		Route::group(array('before'=>'csrf'), function(){

		Route::post('/addthread/{id}', array('uses' => 'ForumController@addThread', 'as' => 'addThread'));
		Route::post('/addComment/{id}', array('uses' => 'ForumController@addComment', 'as' => 'addComment'));
	});

	});
});

Route::group(array('before'=>'guest'), function()
{
    //Can only be accessed by guest user	
	Route::get('user/create', array('uses' =>'UserController@getCreate', 'as' => 'getCreate'));
	Route::get('user/login', array('uses'=> 'UserController@getLogin', 'as' => 'getLogin'));

	Route::group(array('before'=>'crsf'),function(){

		Route::post('user/create', array('uses'=>'UserController@postCreate', 'as'=>'postCreate'));
		Route::post('user/login',array('uses'=>'UserController@postLogin', 'as'=>'postLogin'));
	});
});

Route::group(array('before'=>'auth'), function(){
	//Only meant for authenticated users
	Route::get('user/logout',array('uses'=>'UserController@getLogout', 'as' => 'getLogout'));


});
