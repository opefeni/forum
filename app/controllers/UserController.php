<?php

class UserController extends BaseController
{
	//get view for the registration page
	public function getCreate()
	{
		return View::make('user.register');
	}

	//get view for the login page
	public function getLogin()
	{
		return View::make('user.login');
	}

	public function postLogin()
	{
		$validate = Validator::make(Input::all(), array(
			 	'username' => 'required',
			 	'pass1' => 'required'
			));
		if($validate->fails())
		{
			return Redirect::route('getLogin')->withErrors($validate)->withInput();
		}
		else
		{
			$remember = (Input::has('remember')) ? true : false ;
			$auth = Auth::attempt(array(
                    //field name in db = variable
					'username' => Input::get('username'),
					'password' => Input::get('pass1')
				), $remember);
			if($auth)
			{
				return Redirect::intended('/');
			}
			else
			{
				return Redirect::route('getLogin')->with('fail','Invalid Credentials. Please try again');
			}
		}
	}

	public function postCreate()
	{
		$validate = Validator::make(Input::all(), array(
				'username' => 'required | min:4 |unique:users',
				'pass1' => 'required | min:6',
				'pass2' => 'required | same:pass1',
				'fullname' => 'required'
				)
			);

		  

		if($validate->fails())
		{
			return Redirect::route('getCreate')->withErrors($validate)->withInput();
		}
		else
		{
			$user = new User();
			$user->username = Input::get('username');
			$user->fullname = Input::get('fullname');
			$user->password = Hash::make(Input::get('pass1'));
		
			if($user->save())
			{
				return Redirect::route('getHome')->with('success','You have successfully registered. You can now login');
			}
			else
			{
				return Redirect::route('getHome')->with('fail','An error has occured during user creation. Please try again');
			}
		}
	}
	public function getLogout(){
		Auth::logout();
		return Redirect::route('getHome');
	}

	public function getRememberToken()
	{
		return $this->remember_token;
	}

	public function setRememberToken($remember_token)
	{
		$this->remember_token = $remember_token;
	}

	public function getRememberTokenName()
	{
		return 'remember_token';
	}
	public function isAdmin(){
		return $this->isAdmin();
	}
}