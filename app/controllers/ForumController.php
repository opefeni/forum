<?php

class ForumController extends BaseController{
	
	public function index(){

		$groups = ForumGroup::all();
		$category = ForumCategory::all();
		return View::make('forum.index')->with('groups',$groups)->with('category',$category);
	}
	public function category($id){

        $category = ForumCategory::find($id);
        if($category == null)
        {
        	return Redirect::route('forum-home')->with('fail','Forum Category doesn\'t not exist.');
        }
        $threads = $category->thread()->get();

		return View::make('forum.category')->with('category',$category)->with('threads',$threads);
	}
	public function thread($id){
		return View::make('forum.newThread')->with('id',$id);
	}

	public function threadView($id)
	{
		$thread = ForumThread::find($id);
		if($thread == null)
		{
			return Redirect::route('forum-home')->with('fail','Thread does not exist');
		}
		$author = $thread->author()->first()->username;
		return View::make('forum.thread')->with('thread',$thread)->with('author',$author);
	}
	public function addThread($id)
	{
		$category = ForumCategory::find($id);
		if($category == null)
		{
			return Redirect::route('addThread',$id)->with('fail','Forum Category doesn\'t not exist');
		}
		$validate= Validator::make(Input::all(), array(
				'title' => 'required|unique:forum_threads,title|min:3',
				'body'  => 'required|max:65000'
			));
		if($validate->fails())
		{
			return Redirect::route('forum-home')->withInput()->withErrors($validate)->with('fail','Error ');
		}

		$thread = new ForumThread();
		$thread->title  = Input::get('title');
		$thread->body = Input::get('body');
		$thread->author_id = Auth::User()->id;
		$thread->category_id = $id;
		$thread->group_id = $category->group_id;

		if($thread->save())
			{
				return Redirect::route('forum-home')->with('success','Thread was created successfully');
			}
			else
			{
				return Redirect::route('forum-home')->with('success','An error occurred while creating thread. Please try again');
			}
	}
	public function addGroup()
	{
		$validate= Validator::make(Input::all(), array(
				'group_name' => 'required|unique:forum_groups,title'
			));
		
		if($validate->fails())
		{
			return Redirect::route('forum-home')->withInput()->withErrors($validate)->with('modal','#group_form');

		}
		else
		{
			$group = new ForumGroup();
			$group->title= Input::get('group_name');
			$group->author_id = Auth::user()->id;

			if($group->save())
			{
				return Redirect::route('forum-home')->with('success','Group was created successfully');
			}
			else
			{
				return Redirect::route('forum-home')->with('success','An error occurred while creating a group. Please try again');
			}
		}

	}

	public function addCategory()
	{
		$validate = Validator::make(Input::all(), array(
				'category_name' => 'required|unique:forum_category,title',
				'group_id' => 'required|unique:forum_category,title'
			));
		if($validate->fails())
		{
			return Redirect::route('forum-home')->withInput()->withErrors($validate)->with('modal','#categoryForm');
		}
		else
		{
			$cat = new ForumCategory();
			$cat->title = Input::get('category_name');
			$cat->group_id = Input::get('group_id');
			$cat->author_id = Auth::user()->id;

			if($cat->save())
			{
				return Redirect::route('forum-home')->with('success','Category was created successfully');
			}
			else
			{
				return Redirect::route('forum-home')->with('success','An error occurred while creating a category. Please try again');
			}
		}
	}

	public function deleteGroup($id)
	{



		$group = ForumGroup::find($id);
		if($group == null)
		{
			return Redirect::route('forum-home')->with('fail','Group doesn\'t not exist.' );
		}
		$category = $group->category();
		$thread = $group->thread();
		$comment = $group->comment();

		$delT=true;
		$delCa = true;
		$delCo = true;

		if($category->count() > 0)
		{
			$delCa = $category->delete();
		}
		if($thread->count() > 0)
		{
			$delT = $thread->delete();
		}
		if($comment->count() > 0)
		{
			$delCo = $comment->delete();
		}
		if($category && $thread && $comment && $group->delete())
		{
			return Redirect::route('forum-home')->with('success','Group deleted successfully');
		}
		else
		{
			return Redirect::route('forum-home')->with('fail','Error occurred while deleting group');
		}
	}
		public function deleteCategory($id)
	{

		$category = ForumCategory::find($id);
		if($category == null)
		{
			return Redirect::route('forum-home')->with('fail','category doesn\'t not exist.' );
		}
		$thread = $category->thread();
		$comment = $category->comment();

		$delT=true;
		$delCo = true;

		if($thread->count() > 0)
		{
			$delT = $thread->delete();
		}
		if($comment->count() > 0)
		{
			$delCo = $comment->delete();
		}
		if($delT && $delCo && $category->delete())
		{
			return Redirect::route('forum-home')->with('success','category deleted successfully');
		}
		else
		{
			return Redirect::route('forum-home')->with('fail','Error occurred while deleting category');
		}
	}
	public function addComment($id)
	{
		$thread = ForumThread::find($id);
		if($thread == null)
		{
			return Redirect::route('forum-thread')->with('fail','Thread not available');
		}
		$validate = Validator::make(Input::all(), array(
				'body' => 'required',
			));
		if($validate->fails())
		{
			return Redirect::route('forum-thread')->withInput()->withErrors($validate);//->with('modal','#categoryForm');
		}
		else
		{
			$comment = new ForumComment();
			$comment->body = Input::get('body');
			$comment->author_id = Auth::user()->id;
			$comment->thread_id = $id;
			$comment->category_id = $thread->category_id;
			$comment->group_id = $thread->group_id;

			if($comment->save())
			{
				return Redirect::route('forum-thread-view',$id)->with('success','Category was created successfully');
			}
			else
			{
				return Redirect::route('forum-thread-view',$id)->with('success','An error occurred while creating a category. Please try again');
			}
		}
	}
}