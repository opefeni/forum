<?php

class ForumTableSeeder extends Seeder{

	public function run(){

		User::create(array(
			  'fullname' => 'Forum Administrator',
			  'username' => 'admin',
			  'password' => '$2y$10$w4/vNeDnfB38Ji5mRZW2DuljqoQNEc1UXmdLDA5wzjrZ5dcPzJlhG' //default password : password
			  'isAdmin' => 1
			));
		ForumGroup::create(array(
				'title' => 'General Discussion',
				'author_id' => 1
			));

		ForumCategory::create(array(
				'group_id' => 1,
				'author_id' => 1,
				'title' => 'Laravel Framework'
			));
		ForumCategory::create(array(
				'group_id' => 1,
				'author_id' => 1,
				'title' => 'CakePhp Framework'
			));
	}
}