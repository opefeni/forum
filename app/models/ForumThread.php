<?php

class ForumThread extends Eloquent {

	protected $table = 'forum_threads';

	public function group()
	{
		return $this->BelongsTo('ForumGroup');
	}

	public function category()
	{
		return $this->BelongsTo('ForumCategory');
	}
	public function Comment()
	{
		return $this->hasMany('ForumComment','category_id');
	}
	public function author()
	{
		return $this->hasOne('User','id');
	}
}