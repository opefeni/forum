<?php

class ForumComment extends Eloquent {

	protected $table = 'forum_comments';

	public function group()
	{
		return $this->BelongsTo('ForumGroup');
	}

	public function category()
	{
		return $this->BelongsTo('ForumCategory');
	}
	public function Thread()
	{
		return $this->BelongsTo('ForumThread');
	}
	public function author()
	{
		return $this->BelongsTo('User');
	}
}