<?php

class ForumCategory extends Eloquent {

	protected $table = 'forum_category';

	public function group()
	{
		return $this->BelongsTo('ForumGroup');
	}

	public function Thread()
	{
		return $this->hasMany('ForumThread','category_id');
	}
	public function Comment()
	{
		return $this->hasMany('ForumComment','category_id');
	}
}