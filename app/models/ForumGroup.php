<?php

class ForumGroup extends Eloquent {

	protected $table = 'forum_groups';

	public function category()
	{
		return $this->hasMany('ForumCategory','group_id');
	}

	public function Thread()
	{
		return $this->hasMany('ForumThread','group_id');
	}
	public function Comment()
	{
		return $this->hasMany('ForumComment','group_id');
	}
}